import setuptools
import autogram
from distutils.core import setup
import os
if os.path.exists(os.path.normpath("autogram/_generated")):
    setup(name='Autogram',
          version=autogram.__version__,
          description='Automatically generated python lib',
          author='OctoNezd',
          author_email='nezd@protonmail.com',
          url='http://octonezd.me/autogram',
          packages=['autogram', 'autogram/utils', 'autogram/_generated', 'autogram/_generated/classes', "autogram/shims"],
          install_requires=["requests"]
          )
else:
    raise RuntimeError(
        "Can't find generated files! Please read BUILDING.md")
