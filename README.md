# AutoGram

An automatically-generated Telegram library

## Example:

```
import autogram
# You can easily specify proxy for all requests.
# If you don't need it - just don't define it.
bot = autogram.Bot(TOKEN, PROXY)
me = bot.getMe()
# Every object can be accessed like a class...
print("My username:", me.username)
# ...and like a dictionary!
print("My username:", me["username"])
# Don't like camelCase? No problem, we got you covered!
me = bot.get_me()
print("My username:", me.username)
print("My username:", me["username"])
```

## Building

Read the [Building manual](BUILDING.md)

## TODO list

[Issues with TODO tag](https://gitlab.com/OctoNezd/autogram/issues?label_name%5B%5D=TODO)