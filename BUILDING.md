# Building Autogram

## Requirements for building

```
BeautifulSoup4
Lxml
```

## Building

0. Install requirements.
1. Download Telegram bot documentation page and put it into autogram/ folder with name tgbotapi.html
2. Run `_gen.py`
