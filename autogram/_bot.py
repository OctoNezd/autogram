import requests
import imghdr
from io import BytesIO
import json
import re
import logging
import autogram


def convert(name):
    s1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', name)
    return re.sub('([a-z0-9])([A-Z])', r'\1_\2', s1).lower()


def arg_escape(arg):
    if arg in ["from"]:
        return arg + "_"
    else:
        return arg


def clean_args(args):
    args_new = []
    for arg in args:
        if isinstance(arg, autogram.utils.DictClass):
            arg = clean_kwargs(arg)
        if isinstance(arg, list):
            arg = clean_args(arg)
        args_new.append(arg)
    return args_new


def clean_kwargs(kwargs):
    kwargs_new = {}
    for k, v in kwargs.items():
        if v is None:
            continue
        if isinstance(v, list):
            v = clean_args(v)
        if isinstance(v, autogram.utils.DictClass):
            v = clean_kwargs(v)
        if k in KWARG_OVERRIDE:
            kwargs_new[k] = KWARG_OVERRIDE[k](v)
        else:
            kwargs_new[k] = v
    return kwargs_new


def create_function_proxy(self, function):
    def func_proxy(*args, **kwargs):
        return function(self, *args, **kwargs)
    return func_proxy


def reply_markup_override(rmarkup):
    if isinstance(rmarkup, dict) or isinstance(rmarkup, list):
        rmarkup = json.dumps(rmarkup)
    return rmarkup


KWARG_OVERRIDE = {"reply_markup": reply_markup_override,
                  "results": reply_markup_override}


class InvalidField:
    pass


class BaseBot():
    def __init__(self, token, proxy=None, shim=None):
        self.token = token
        if proxy is not None:
            self.proxy = {"https": proxy}
        else:
            self.proxy = None
        self.logger = logging.getLogger("autogram.Bot - Not loaded yet")
        self.shim = shim
        for function, inner in self.TBOTAPI_FUNCTIONS.items():
            setattr(self, convert(function),
                    create_function_proxy(self, inner["function"]))
        try:
            me = self.getMe()
        except autogram.errors.Unauthorized:
            raise autogram.errors.InvalidToken("Invalid token provided!")
        else:
            self.logger = logging.getLogger(
                "autogram.Bot - %s" % me.username)
        self.logger.info("Bot instance created OK")

    def apply_shim(self, cls):
        if self.shim is not None:
            if type(cls).__name__ in self.shim:
                self.shim[type(cls).__name__](cls, self)
            if "@a" in self.shim:
                self.shim["@a"](cls, self)

    def _escape(self, cls, classname, name=None, embed=False):
        # dont judge me please, i know it sucks
        resultembedclass = {}
        if embed:
            parentclass = autogram.CLASSES[classname]
            try:
                classname = parentclass.__init__.__annotations__[name]
            except KeyError:
                self.logger.critical(
                    "Failed to convert %s to class! This usually"
                    " means outdated library version"
                    " or undocumented Telegram-side change.", name)
                return InvalidField
        self.logger.debug("class %s", classname)
        if isinstance(cls, dict):
            for k, v in cls.items():
                if isinstance(v, dict):
                    escaped = self._escape(
                        v, classname=classname, name=arg_escape(k), embed=True)
                    if not isinstance(escaped, InvalidField):
                        self.apply_shim(escaped)
                        resultembedclass[arg_escape(k)] = escaped
                elif isinstance(v, list):
                    n_l = []
                    for item in v:
                        n_l.append(self._escape(
                            item, classname=classname, name=arg_escape(k), embed=True))
                    resultembedclass[arg_escape(k)] = n_l
                else:
                    resultembedclass[arg_escape(k)] = v
            result = autogram.CLASSES[classname](**resultembedclass)
            self.apply_shim(result)
        elif isinstance(cls, list):
            result = []
            for item in cls:
                cls_res = autogram.CLASSES[classname](
                    **self._escape(item, classname=classname))
                self.apply_shim(cls_res)
                result.append(cls_res)
        return result

    # def embed_object_escape(self, embedclass, classname, name):
    #     parentclass = autogram.CLASSES[classname]
    #     try:
    #         embedclassname = parentclass.__init__.__annotations__[name]
    #     except KeyError:
    #         self.logger.critical(
    #             "Failed to convert %s to class! This usually means outdated library version or undocumented Telegram-side change. Skipping it.", name, exc_info=True)
    #         return InvalidField
    #     resultembedclass = self._escape()
    #     res = autogram.CLASSES[embedclassname](**resultembedclass)
    #     return res

    def object_escape(self, result, funcname=None, classname=None):
        classname = self.TBOTAPI_FUNCTIONS[funcname]["returns"]
        esc = self._escape(result, classname=classname)
        # for k, v in esc.items():
        #     escaped = self.embed_object_escape(
        #         v, classname=cclass, name=arg_escape(k))
        #     if not isinstance(escaped, InvalidField):
        #         self.apply_shim(escaped)
        #         esc[arg_escape(k)] = escaped
        return esc

    def file_upload_prep(self, kwargs, funcname):
        function_annotations = self.TBOTAPI_FUNCTIONS[funcname]["function"].__annotations__
        files = None
        file = None
        for var, annotation in function_annotations.items():
            if "InputFile" in annotation:
                if var in kwargs:
                    self.logger.debug("File uploaded needed!")
                    if isinstance(kwargs[var], bytes):
                        file = BytesIO(kwargs[var])
                    if hasattr(kwargs[var], "read"):
                        file = kwargs[var]
                    if file is not None:
                        if hasattr(file, "name"):
                            filename = file.name
                        elif hasattr(file, "read"):
                            imgtype = imghdr.what(None, file.read())
                            if imgtype is not "":
                                filename = "file." + imgtype
                            else:
                                filename = "file.bin"
                            file.seek(0)
                        else:
                            filename = file
                        kwargs[var] = "attach://" + filename
        if file is not None:
            files = {filename: file}
            self.logger.debug("Uploading file %s with name %s", file, filename)
        else:
            self.logger.debug("File upload is not required for request")
        return files, kwargs

    def request(self, funcname, kwargs):
        del kwargs["self"]
        kwargs = clean_kwargs(kwargs)
        files, kwargs = self.file_upload_prep(kwargs, funcname)
        self.logger.debug("Calling %s with %s", funcname, kwargs)
        r = requests.post("https://api.telegram.org/bot%s/%s" %
                          (self.token, funcname), params=kwargs, files=files, proxies=self.proxy).json()
        if r["ok"] != True:
            if r["error_code"] in autogram.errors.ERROR_CODES:
                raise autogram.errors.ERROR_CODES[r["error_code"]](
                    r["description"])
            else:
                raise autogram.errors.TelegramError(r["description"])
        if isinstance(r["result"], list) or isinstance(r["result"], dict):
            result_escaped = r["result"]
            if not self.TBOTAPI_FUNCTIONS[funcname]["returns"] == "None":
                result_escaped = self.object_escape(result_escaped, funcname)
        else:
            result_escaped = r["result"]
        self.logger.debug(result_escaped)
        return result_escaped

    TBOTAPI_FUNCTIONS = {}
