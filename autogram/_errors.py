class TelegramError(RuntimeError):
    pass


class Unauthorized(TelegramError):
    pass


class InvalidToken(TelegramError):
    pass

class BadRequest(TelegramError):
    pass

ERROR_CODES = {401: Unauthorized, 400: BadRequest}
