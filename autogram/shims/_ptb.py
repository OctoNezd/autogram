import requests
import inspect


def wire_function(cls, function_name, function, func_kwargs):
    def wired(*args, **kwargs):
        kwargs.update(func_kwargs)
        return function(*args, **kwargs)
    setattr(cls, function_name, wired)


def message_shim(message, bot):
    setattr(message, "from_user", message.from_)
    setattr(message, "chat_id", message.chat.id)
    edit_kwargs = {"chat_id": message.chat.id,
                   "message_id": message.message_id}
    wire_function(message, "delete", bot.delete_message,
                  edit_kwargs)
    wire_function(message, "edit_caption",
                  bot.edit_message_caption, edit_kwargs)
    wire_function(message, "edit_media", bot.edit_message_media, edit_kwargs)
    wire_function(message, "edit_reply_markup",
                  bot.edit_message_reply_markup, edit_kwargs)
    wire_function(message, "edit_text", bot.edit_message_text, edit_kwargs)
    reply_kwargs = {"chat_id": message.chat.id,
                    "reply_to_message_id": message.message_id}
    wire_function(message, "reply_animation", bot.send_animation, reply_kwargs)
    wire_function(message, "reply_audio", bot.send_audio, reply_kwargs)
    wire_function(message, "reply_contact", bot.send_contact, reply_kwargs)
    wire_function(message, "reply_document", bot.send_document, reply_kwargs)
    wire_function(message, "reply_html", bot.send_message, {
                  "parse_mode": "HTML", **reply_kwargs})
    wire_function(message, "reply_location", bot.send_location, reply_kwargs)
    wire_function(message, "reply_markdown", bot.send_message, {
                  "parse_mode": "MARKDOWN", **reply_kwargs})
    wire_function(message, "reply_media_group",
                  bot.send_media_group, reply_kwargs)
    wire_function(message, "reply_photo", bot.send_photo, reply_kwargs)
    wire_function(message, "reply_sticker", bot.send_sticker, reply_kwargs)
    wire_function(message, "reply_text", bot.send_message, reply_kwargs)
    wire_function(message, "reply_venue", bot.send_venue, reply_kwargs)
    wire_function(message, "reply_video_note",
                  bot.send_video_note, reply_kwargs)
    wire_function(message, "reply_voice", bot.send_video_note, reply_kwargs)


def chat_shim(chat, bot):
    setattr(chat, "CHANNEL", "channel")
    setattr(chat, "GROUP", "group")
    setattr(chat, "PRIVATE", "private")
    setattr(chat, "SUPERGROUP", "supergroup")
    default_func_kwarg = {"chat_id": chat.id}
    wire_function(chat, "get_administrators",
                  bot.get_chat_administrators, default_func_kwarg)
    wire_function(chat, "get_member", bot.get_chat_member,
                  default_func_kwarg)
    wire_function(chat, "get_members_count",
                  bot.get_chat_members_count, default_func_kwarg)
    wire_function(chat, "kick_member", bot.kick_chat_member,
                  default_func_kwarg)
    wire_function(chat, "send_action", bot.send_chat_action,
                  default_func_kwarg)
    wire_function(chat, "send_animation",
                  bot.send_animation, default_func_kwarg)
    wire_function(chat, "send_audio", bot.send_audio, default_func_kwarg)
    wire_function(chat, "send_document", bot.send_document, default_func_kwarg)
    wire_function(chat, "send_message", bot.send_message, default_func_kwarg)
    wire_function(chat, "send_photo", bot.send_photo, default_func_kwarg)
    wire_function(chat, "send_sticker", bot.send_sticker, default_func_kwarg)
    wire_function(chat, "send_video", bot.send_video, default_func_kwarg)
    wire_function(chat, "send_video_note",
                  bot.send_video_note, default_func_kwarg)
    wire_function(chat, "send_voice", bot.send_voice, default_func_kwarg)
    wire_function(chat, "unban_member",
                  bot.unban_chat_member, default_func_kwarg)
    if chat.username is not None:
        setattr(chat, "link", "https://t.me/" + chat.username)


def all_shim(cls, bot):
    # Python-Telegram-Bot always puts bot field
    def to_dict(cls=cls):
        n_d = {}
        for k, v in cls.items():
            if k == "bot":
                continue
            elif isinstance(v, dict):
                n_d[k] = to_dict(v)
            elif isinstance(v, list):
                n_l = []
                for item in v:
                    n_l.append(to_dict(item))
                n_d[k] = n_l
            elif not (callable(v) or inspect.isclass(v)):
                n_d[k] = v
        return n_d
    setattr(cls, "bot", bot)
    setattr(cls, "to_dict", to_dict)


def file_download_shim(cls, bot):
    def download_file(custom_path=None, out=None, timeout=None):
        if custom_path is not None or out is not None:
            file = bot.getFile(cls.file_id)
            r = requests.get("https://api.telegram.org/file/bot%s/%s" %
                             (bot.token, file.file_path), proxies=bot.proxy)
            if custom_path is not None:
                with open(custom_path, 'wb') as f:
                    f.write(r.content)
                    f.close()
                    return custom_path
            elif out is not None:
                out.write(r.content)
                return out
        else:
            raise ValueError("Custom_path or out are not specified")

    setattr(cls, "download", download_file)


PythonTelegramBotShim = {"Message": message_shim,
                         "Chat": chat_shim,
                         "PhotoSize": file_download_shim,
                         "Audio": file_download_shim,
                         "Document": file_download_shim,
                         "Video": file_download_shim,
                         "Animation": file_download_shim,
                         "Voice": file_download_shim,
                         "VideoNote": file_download_shim,
                         "File": file_download_shim,
                         "Sticker": file_download_shim,
                         "PassportFile": file_download_shim,
                         "@a": all_shim}
