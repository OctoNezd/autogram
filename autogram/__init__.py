try:
    from autogram._generated.botapiver import BOT_API_VER
    from autogram._generated.classes import *
    from autogram._generated._bot import Bot
except ImportError as e:
    raise RuntimeError("Can't load generated classes - did you run _gen.py?")
from autogram import _errors as errors
from autogram import utils
__version__ = "0.2.2-" + BOT_API_VER
import logging
logging.getLogger("autogram - system").debug("autogram %s imported", __version__)
