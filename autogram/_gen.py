from string import Template
import operator
import textwrap
import os
from bs4 import BeautifulSoup


def remove_non_ascii(s): return "".join(i for i in s if ord(i) < 128)


def arg_escape(arg):
    if arg in ["from"]:
        return arg + "_"
    else:
        return arg


FUNCTION_OVERRIDES = ["getMe"]

function_template = Template("""
def $funcname($kwargs):
    \"""
$docstring
    ""\"
    return self.request("$funcname", kwargs=locals())
""".strip() + "\n\n")


class_template = Template("""
from autogram.utils.dictclass import DictClass


class $classname(DictClass):
    def __init__(self,
$kwargs,
                 **kwargs):
        \"""
$docstring
        ""\"
        lcls = dict(locals())
        del lcls["kwargs"]
        vars = lcls
        vars.update(kwargs)
        DictClass.__init__(self, vars)
        del self["self"]
""".strip())

base_bot = """
from autogram._bot import BaseBot
import autogram

class Bot(BaseBot):
"""


def wrap_kwargs(kwargs_list, offset_str="    def __init__("):
    kwargs_list = list(kwargs_list)
    if kwargs_list[0] == "self":
        kwargs = [kwargs_list[0]]
        offset = 1
    else:
        kwargs = []
        offset = 0
    for arg in kwargs_list[offset:]:
        kwargs.append(textwrap.indent(arg, " " * len(offset_str)))
    return ",\n".join(kwargs)


def wrap_desc(desc):
    desc_list = textwrap.wrap(desc, 60)
    desc = desc_list[0]
    for desc_part in desc_list[1:]:
        desc += "\n" + textwrap.indent(desc_part, " " * 4)
    return desc


def docstring_wrap_argument(argument):
    wrap = argument.find_all("td")[0].text + ": "
    wrap += wrap_desc(argument.find_all("td")[-1].text)
    return textwrap.indent(wrap + "\n", " " * 4)


def main():
    os.makedirs("_generated/classes", exist_ok=True)
    print("Start generating Telegram API")
    with open("tgbotapi.html", 'r', encoding="utf-8") as f:
        soup = BeautifulSoup(f.read(), "lxml")
    bapiver = soup.find_all("strong")[2].text
    print("API Version:", bapiver)
    with open("_generated/botapiver.py", 'w') as f:
        f.write("BOT_API_VER='%s'" % bapiver)
    functions = {}
    classes = []
    with open("_generated/_bot.py", 'w') as f:
        f.write(base_bot)
    for item in soup.find_all("h4"):
        if " " not in item.text:
            if item.text[0].islower():
                print("Function:", item.text)
                with open("_generated/_bot.py", 'a') as f:
                    description = item.find_next_sibling()
                    docstring = wrap_desc(description.text)
                    docstring += "\nArgs:\n"
                    kwargs = {"self": 0}
                    if item.text not in FUNCTION_OVERRIDES:
                        for argument in item.find_next_sibling("table").find_all("tr")[1:]:
                            arg = arg_escape(argument.find_all("td")[0].text)
                            docstring += docstring_wrap_argument(argument)
                            arg += ": '" + argument.find_all("td")[1].text + "'"
                            if argument.find_all("td")[2].text == "Yes":
                                kwargs[arg] = 0
                            else:
                                kwargs[arg + " = None"] = 1
                    kwargs = dict(
                        sorted(kwargs.items(), key=operator.itemgetter(1))).keys()
                    docstring = textwrap.indent(docstring, " " * 4)
                    returns = None
                    for link in description.find_all("a"):
                        if link.text[0].isupper():
                            returns = link.text
                    function = function_template.substitute(
                        docstring=docstring,
                        funcname=item.text,
                        kwargs=wrap_kwargs(kwargs, "def %s(" % item.text),
                        returns=returns)
                    with open("_generated/_bot.py", 'a') as f:
                        f.write(textwrap.indent(
                            remove_non_ascii(function), " " * 4))
                functions[item.text] = returns
            else:
                print("Class:", item.text)
                with open("_generated/classes/" + item.text + ".py", 'w') as f:
                    description = item.find_next_sibling()
                    docstring = wrap_desc(description.text)
                    docstring += "\nArgs:\n"
                    kwargs = []
                    if item.text not in FUNCTION_OVERRIDES:
                        for argument in item.find_next_sibling("table").find_all("tr")[1:]:
                            arg = arg_escape(argument.find_all("td")[0].text)
                            docstring += docstring_wrap_argument(argument)
                            argtype = argument.find_all("td")[1].find("a")
                            if argtype is not None:
                                argtype = argtype.text
                            else:
                                argtype = "Unknown"
                            defvalue = "None"
                            if ", must be" in argument.find_all("td")[-1].text and argument.find_all("td")[-1].find_all("em") != []:
                                defvalue = "'" + argument.find_all(
                                    "td")[-1].find_all("em")[-1].text + "'"
                            kwargs.append(
                                arg + ": '" + argtype + "' = " + defvalue)
                        docstring = textwrap.indent(docstring, " " * 8)
                        f.write(remove_non_ascii(class_template.substitute(
                            docstring=docstring, classname=item.text, kwargs=wrap_kwargs(kwargs)) + "\n"))
                classes.append(item.text)
    with open("_generated/classes/__init__.py", 'w') as f:
        f.write("CLASSES = {}\n")
        for class_ in classes:
            f.write("from autogram._generated.classes.%s import %s\n" %
                    (class_, class_))
            f.write('CLASSES["%s"] = %s\n' % (class_, class_))
    with open("_generated/_bot.py", "a") as f:
        print("Writing function returns...")
        f.write(" " * 4 + "TBOTAPI_FUNCTIONS = {}\n")
        for function, returns in functions.items():
            f.write(" " * 4 + "TBOTAPI_FUNCTIONS['%s'] = {'returns':'%s', 'function':%s}\n" % (
                function, returns, function))


if __name__ == '__main__':
    main()
